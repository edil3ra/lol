import Vue from 'vue'
import Router from 'vue-router'
import Heroes from '@/components/Heroes'
import Hero from '@/components/Hero'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      name: 'home',
      path: '/',
      redirect: '/heroes'
    },
    {
      name: 'heroes',
      path: '/heroes',
      component: Heroes
    },
    {
      name: 'hero',
      path: '/heroes/:id',
      component: Hero
    }
  ]
})
