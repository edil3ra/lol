import axios from 'axios'
import * as config from './config'

const http = axios.create({
  baseURL: `http://${config.api.host}:${config.api.port}`
})

function getHeroes (params = {}) {
  return http.request({
    method: 'get',
    url: '/heroes',
    params: params
  }).then((response) => {
    return response.data
  })
}

function getHero (id) {
  return http.request({
    method: 'get',
    url: `/heroes/${id}`
  }).then((response) => {
    if (response.status === 204) {
      return Promise.reject(new Error({
        status: response.status,
        message: `hero not found at ${id}`
      }))
    }
    return response.data
  })
}

export default {
  instance: http,
  getHeroes,
  getHero
}
