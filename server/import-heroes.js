const axios = require('axios')
const MongoClient = require('mongodb').MongoClient;
const config = require('./config')
const mongoURL = `mongodb://${config.mongo.host}:${config.mongo.port}`
const dbName = config.mongo.db


module.exports = function exportHeroesToMongo() {
  console.log("Prepare export")
  return axios.get(config.lolChampionURL)
    .then(response =>  {
      console.log("Resource Fetched")
      const heroes = response.data
      return heroes
    })
    .then(heroes => {
      return  MongoClient.connect(mongoURL, { useNewUrlParser: true })
        .then((client) => {
          const heroesTableName = 'heroes'
          const db = client.db(dbName)
          console.log("Connected to database");
          db.collection(heroesTableName).drop().catch(() => {})
          console.log(`${heroesTableName} collection dropped`);

          db.collection(heroesTableName).insertMany(heroes)
          console.log(`${heroes.length} ${heroesTableName} inserted`);
          client.close();
        })
    })
    .catch(err => {
      console.log(err)
    })
}

