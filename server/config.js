const lolChampionURL = 'https://raw.githubusercontent.com/ngryman/lol-champions/master/champions.json'
const limitPageDefault = 50
const process = require('process')

const mongo = {
  host: process.env.NODE_ENV === 'development'? 'loldb' : 'loldbprod' ,
  port: 27017,
  db: 'lol',
  authSource: 'admin',
}

module.exports = {
  mongo: mongo,
  lolChampionURL,
  limitPageDefault
}


