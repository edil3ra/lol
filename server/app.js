const Koa = require('koa')
const mongo = require('koa-mongo')
var Router = require('koa-router')
const cors = require('@koa/cors')

const config = require('./config')
const exportHeroesToMongo = require('./import-heroes.js')

const app = new Koa()
var router = new Router()


/* MIDDLEWARE */
// logger
app.use(async (ctx, next) => {
  await next()
  const rt = ctx.response.get('X-Response-Time')
  console.log(`${ctx.method} ${ctx.url} - ${rt}`)
})

// mongo
app.use(mongo(config.mongo))

// cors
app.use(cors())


/* ROUTER */
 router.get('/heroes', async (ctx, next) => {
   const page = parseInt(ctx.request.query.page) || 0
   const limit = parseInt(ctx.request.query.limit) || config.limitPageDefault
   
   const result = await ctx.db.collection('heroes')
     .find()
     .skip(page * limit)
     .limit(limit)
     .toArray()

   ctx.body = result
})

 router.get('/heroes/:id', async (ctx, next) => {
  const result  = await ctx.db.collection('heroes').findOne({id: ctx.params.id})
  ctx.body = result
})

app.use(router.routes())

exportHeroesToMongo().then(() => {
  console.log('App listen to 3000')
  app.listen(3000)  
})
