# Description
    Small application that display a list of league of legend heroes with their details
    It could be improve but it's good enough for a demo
    
# thechnologies
    
## Client
    + Vuejs, vue-router
    + materialize
    + axios

## Server
    + koa
    + mongodb
    
### details
    + server have 2 routes /heroes and /heroes/:id
    + When the server start, it fetch the list of heroes from the'https://raw.githubusercontent.com/ngryman/lol-champions/master/champions.json' and put the data in the database.
    
# Docker
## Dev
### Launch
    docker-compose up
### Details
    + server container: lolserver (start api server at: 3000)
    + client container: lolclient (start dev server at: 8000)
    + db container: loldb (start db at: 27017)

## Prod
### Launch
    docker-compose -f docker-compose.prod.yml up
    
### Details
    + server container: lolserverprod (start api server at: 3000)
    + client container: lolclientprod (build application, start nginx server at: 80)
    + db container: loldbprod (start db at: 27017)
